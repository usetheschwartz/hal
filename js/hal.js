function Tag(text) {
    /*
     * This is the base tag object, it does most of the parsing
     */
    this.raw = text;
    this.attr = '';     //this holds the full attribute contents.
    this.attrs = [];    //this holds key/value pairs of attributes.
    this.text = '';     //this holds element text (if any).
    this.tag = '';      //this holds the tag name itself.
    this.parse = function() {
        /*
         * This function does the actual parsing for the tag object
         */
        pattern = /(.*?) attr\((.*?)\) text\((.*?)\)$|(.*?) attr\((.*?)\)$|(.*?) text\((.*?)\)$|(.*?)$/m;

        try {
            regex = new RegExp(pattern);
            if(regex.test(this.raw)) {
                /*
                 * RegExp was returning all of the above matches that it tried,
                 * Including the empty ones. this picks out the ones that correspond
                 * to specific parts of a Tag and groups them into an array so that 
                 * the sort function can sort them.
                 */
                var match = regex.exec(this.raw);
                var tag_matches = [
                    match[1],match[4],match[6],match[8]
                ];
                var attr_matches = [
                    match[2],match[5]
                ];
                var text_matches = [
                    match[3],match[7]
                ];
            } else {
                throw new Error("Cannot use pattern on input: "+this.raw);
            }
        } catch(e) {
            console.log(e);
        }
        
        var sort = function(a) {
            /*
             * This function sorts out the blank values from the
             * arrays defined above.
             */
            var length = a.length;
            for(var i = 0; i < length; i++) {
                try {
                    if(a[i] === undefined || a[i] === '') {
                        throw new Error();
                    } else {
                        return a[i];
                    }
                } catch(e) {
                    console.log(e);
                }
            }
        };
        /*
         * The rest of this function uses sort() to pick correct values then
         * and parses them and adds their respective values to the object.
         */
        this.tag = sort.call(this, tag_matches);
        
        try {
            this.attr = sort.call(this, attr_matches);
            var attr_list = sort.call(this, attr_matches).split(' ');
            var l = attr_list.length;
            for(var i = 0; i < l; i++) {
                tmp = new RegExp(/(.+?)='(.+?)'/).exec(attr_list[i]); //this populates the 'this.attrs' array.
                this.attrs[i] = [
                    tmp[1],tmp[2]
                ];
            }
        } catch(e) {
            console.log(e);
        }
        
        try {
            this.text = sort.call(this, text_matches);
        } catch(e) {
            console.log(e);
        }
    
        return match;
    };
    
    this.parse();
    
    return this;
};

function Element(src, index, parent, children) {
    /*
     * this is the Element object which extends Tag with parents, 
     * children, and depth variables which help to parse the document
     * and later help to generate html.
     */
    this.prototype = Object.create(Tag.prototype);
    Tag.call(this, src.trim());
    this.depth = 0;
    for(var c in src) {
        if(src[c] === ' ') {++this.depth;}
        else {break;}
    }
    
    this.parent = parent; // should be an index in the element array
    this.children = children; // should contain indicies of children elements
    this.index = index;
};

function test(item) {
    /*
     * this function evaluates a given 'item' and determines
     * if it has a value.
     */
    if(item === null | item === undefined | item === ''  | item === new Array()) {return false;}
    else {return true;}
};

function Doc(src) {
    /*
     * This is the Doc object. It parses a hal source file and
     * creates html from it.
     */
    this.prep = function(text) { 
        /*
         * this function replaces any tabs with spaces to
         * make parsing easier.
         * 
         */
        return text.replace(/\t/, ' ');
    };
    
    this.findDepth = function(text) {
        /*
         * this function returns the preceding
         * indentation depth of a string
         */
        var depth = 0;
        for(var i in text) {
            if(text[i] === ' ') {++depth;}
            else {break;}
        }
        return depth;
    };
    
    this.cmpws = function(str_1, str_2) {
        /*
         * this function compares the preceding whitespace of two strings
         */
        var ws_1 = this.findDepth(str_1);
        var ws_2 = this.findDepth(str_2);
        if(ws_1 < ws_2) {return 1;}
        else if(ws_1 > ws_2) {
            return -1;
        }
        else {return 0;}
    };
    
    this.parse = function(idx, parent) {
        /*
         * This function tests the last line to see if the
         * current line is indented, de-indented, or the same.
         * It then sets the parent for the new element appropriately
         * and instantiates a new Element() with the given information
         * and adds it to the element array.
         */
        var line = this.lines[idx];     //current line
        var last = this.lines[idx-1];   //last line
        var next = this.lines[idx+1];   //next line

        var ws = this.cmpws(last, line);

        if(ws === 1) { //in case of an indent:
            parent = this.elements.length-1;
            console.log("case 1");
        } else if(ws === -1) { //in case of a de-indent:
            var depth = this.findDepth(line);
            var lastDepth = this.findDepth(last);
            for(var i = 0; i < lastDepth-depth; i++) {
                parent = this.elements[parent].parent;
            }
            console.log("case -1");
        }
        
        //if there isn't an indent or deindent,
        //the parent stays the same.
        
        var e = new Element(line, this.elements.length, parent, []);
        console.log(e);

        if(this.elements.indexOf(e) === -1) { //this prevents duplicate Elements.
            this.elements.push(e);
            console.log(parent);
            this.elements[parent].children.push(this.elements.indexOf(e));
        }

        if(test(next)) { //if the next line is defined:
            this.parse(idx+1, parent);
        }
    };
   
    this.makeHtml = function(idx) {
        /*
         * This function creates HTML from the parsed Element() objects
         * created above.
         */
        var e = this.elements[idx];
        var inners = "";
        
        for(var i in e.children) { //for every child element, retrieve that child's html.
            var j = e.children[i];
            var inner = this.makeHtml(j);
            inners = inners+inner;
        }
        
        if(!test(e.attr)) {
            if(!test(e.text)) {
                if(test(inners)) {var t = "<"+e.tag+">"+inners+"</"+e.tag+">";}
                else {var t = "<"+e.tag+">";}
                console.log(t);
            } else {
                var t = "<"+e.tag+">"+e.text+"</"+e.tag+">";
                console.log(t);
            }
        } else {
            if(!test(e.text)) {
                var t = "<"+e.tag+" "+e.attr+">"+inners+"</"+e.tag+">";
                console.log(t);
            } else {
                var t = "<"+e.tag+" "+e.attr+">"+e.text+"</"+e.tag+">";
                console.log(t);
            }
        }

        return t;
    };
    
    this.output = function() {
        /*
         * this function wraps makeHtml() to only provide the head and body
         * elements. This prevents the root hal element form showing up in
         * the generated html.
         */ 
        for(var i in this.elements) {
            if(this.elements[i].tag === "head") {
                var head = this.makeHtml(i);
            }
            else if(this.elements[i].tag === "body") {
                var body = this.makeHtml(i);
            }
        }
        
        return head+body;
    };
    
    this.raw = this.prep(src);
    this.lines = this.raw.split(/(.+?)(?:$)/m);
    for(var line in this.lines) {
        if(this.lines[line] === ""||"\n") {this.lines.splice(line, 1);}
    }
    console.log(this.lines);
    this.elements = new Array();
    
    this.elements.push(new Element(this.lines[0], 0, null, []));
    this.parse(1, 0);
    
    this.html = this.output();
};

function hal(filename) {
    //get the working directory.
    var dir = location.pathname.substring(0,location.pathname.lastIndexOf('/')+1);
    
    //create a new AJAX request to fetch the source.
    var xhr = new XMLHttpRequest();
    xhr.open("GET", dir+"hal/"+filename, false);
    xhr.send(null);
    var src = xhr.responseText;
    
    //instantiate a new Doc() object and write the generated 
    //html to the browser.
    var doc = new Doc(src);
    var html = doc.html;
    document.write(html);
};